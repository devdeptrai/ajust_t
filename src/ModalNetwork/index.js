import React, { PureComponent } from 'react';
import {
  Modal,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Platform,
  CameraRoll,
  Image,
  Dimensions,
} from 'react-native';

const { width, height } = Dimensions.get('window');

export default class ModalNetwork extends PureComponent {
  componentDidMount() {}
  componentWillReceiveProps(nextProps) {}

  render() {
    const { visible } = this.props;

    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'rgba(0,0,0,0.6)',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <View
          style={{
            height: height * 0.315,
            width: width * 0.933,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#fff',
            borderRadius: 20,
          }}
        >
          <Text
            style={{
              color: 'red',
              fontSize: 16,
              marginBottom: 15,
              margin: 10,
              marginTop: 40,
            }}
          >
            Error!
          </Text>
          <View
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
          >
            <Text style={{ color: '#000', fontSize: 16 }}>
              ネットワークに接続できません
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
