import React, { Component } from 'react';
import {
  View,
  Platform,
  Alert,
  Modal,
  Linking,
  NativeModules,
  AppState
} from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { WebView } from 'react-native-webview';
import Sound from 'react-native-sound';
import Permissions from 'react-native-permissions';
import { AudioRecorder, AudioUtils } from 'react-native-audio';
import CameraModal from './CameraModal';
import WebViewTypes from './WebViewTypes';
import ModalNetwork from './ModalNetwork';
import ImageResizer from 'react-native-image-resizer';
import { statusBarHeight } from './utilities/Transform';
import firebase from 'react-native-firebase';
import type { Notification, NotificationOpen } from 'react-native-firebase';
import DeviceInfo from 'react-native-device-info';

const audioPath = `${
  AudioUtils.DocumentDirectoryPath
}/audio${new Date().getTime()}.aac`;

const EVENT_NETINFO = 'connectionChange';

const URI = 'https://app.alugo.net/';
const CONFIG = 'https://aluereact-release.classfox.co.jp/api/getAppConfigs'

// const { width, height } = Dimensions.get('window');

// function toHalfWidth(strVal) {
//   // 半角変換
//   var halfVal = strVal.replace(/[！-～]/g, function(tmpStr) {
//     // 文字コードをシフト
//     return String.fromCharCode(tmpStr.charCodeAt(0) - 0xfee0);
//   });
//
//   // 文字コードシフトで対応できない文字の変換
//   return halfVal
//     .replace(/”/g, '"')
//     .replace(/’/g, "'")
//     .replace(/‘/g, '`')
//     .replace(/￥/g, '\\')
//     .replace(/　/g, ' ')
//     .replace(/〜/g, '~');
// }

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      paused: false,
      audioPath: audioPath,
      permissionPhoto: undefined,
      permissionRecord: undefined,
      permissionPush: undefined,
      testmodal: false,
      currentTime: 0.0,
      recording: false,
      stoppedRecording: false,
      finished: false,
      open: false,
      playing: false,
      tokenLoaded: false,
      webviewURI: URI,
      webviewLoaded: false,
      appState: AppState.currentState,
      currentTab: -1,
      API: '',
      Config_API: CONFIG,
    };

    this.pushToken = null;
  }
  componentDidUpdate() {}
  componentWillUnmount() {
    Linking.removeEventListener('url', this.handleOpenURL);
    NetInfo.removeEventListener(
      EVENT_NETINFO,
      this.handleFirstConnectivityChange,
    );
    AppState.removeEventListener('change', this.appStateChange);
    // unsubscribe notification listeners
    this.notificationOpenedListener();
    this.notificationListener();
  }

  componentDidMount = async() => {
    this.getConfigs();
    this.registerPushToken();
    Linking.addEventListener('url', this.handleOpenURL);
    setTimeout(() => {
      this.webview.postMessage(WebViewTypes.IS_MOBILE);
    }, 3000);

    this.checkNotificaion();
    this.checkPermission();
    this.checkNetwork();
  };

  getConfigs = async() => {
    const { Config_API } = this.state;
    await fetch(Config_API)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          // webviewURI: 'https://app-staging.alugo.net/',
          // API: 'https://aluereact-staging.classfox.co.jp/api/'

          webviewURI: 'https://app.alugo.net/',
          API: 'https://aluereact-release.classfox.co.jp/api/'
        })
      })
      .catch((error) => {
        this.setState({
          webviewURI: URI,
        })
      });
  };

  checkNotificaion = async() => {
    // Android 8.0 (API Level 26) or higher
    if (Platform.Version >= 26) {
      // Build a channel
      const channel = new firebase.notifications.Android.Channel('general-notification', 'notification', firebase.notifications.Android.Importance.Max);

      // Create the channel
      firebase.notifications().android.createChannel(channel);
    }
    //App is background, listen when notification is clicked
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
      const action = notificationOpen.action;
      const notification: Notification = notificationOpen.notification;
      let data = JSON.parse(notification.data.target);
      if (data.target) {
        this.setState({webviewURI: URI + '?t=' + Date.now() + '#' + data.target});
      }
    });
    //App is closed, listen when notification is clicked
    const notificationOpen: NotificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
        const action = notificationOpen.action;
        const notification: Notification = notificationOpen.notification;
        let data = JSON.parse(notification.data.target);
        if (data.target) {
          this.setState({webviewURI: URI + '?t=' + Date.now() + '#' + data.target});
        }
    }
    this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {

    });
    //Particular notification has been received in foreground
    this.notificationListener = firebase.notifications().onNotification((notification: Notification) => {
      this.webview.postMessage(WebViewTypes.NOTIFCATION);
      //Skip push on coaching tab
      if(this.state.currentTab == 3) {
        return
      }
      if (!notification.data.chat && this.state.appState == 'active') {
        // iOS 9 & below does not support displaying a notification
        // whilst the app is in the foreground
        if (Platform.OS == 'ios' && parseInt(Platform.Version, 10) <= 9) {
          Alert.alert(
            notification.title,
            notification.body,
          );
          return;
        }
        const localNotification = new firebase.notifications.Notification()
          .setTitle(notification.title)
          .setBody(notification.body)
          .setData(notification.data)
          .setSound('default');
        if (Platform.OS === 'android') {
          localNotification
            .android.setChannelId('general-notification')
            .android.setPriority(firebase.notifications.Android.Priority.Max);
        }
        firebase.notifications().displayNotification(localNotification);
      }
    });
    //Data payload in foreground
    this.messageListener = firebase.messaging().onMessage((message) => {

    });
    AppState.addEventListener('change', this.appStateChange.bind(this));
  };

  appStateChange(next) {
    this.setState({ appState: next });
  }

  checkNetwork = () => {
    NetInfo.getConnectionInfo().then(connectionInfo => {
      if (connectionInfo.type === 'none') {
        this.setState({ open: true });
      } else {
        this.setState({ open: false });
      }
    });
    NetInfo.addEventListener(EVENT_NETINFO, this.handleFirstConnectivityChange);
  };
  checkPermission = () => {
    let typeAndroid=['photo', 'microphone'];
    let typeIos=['photo', 'microphone', 'notification'];
    if(Platform.OS === "android") {
      Permissions.checkMultiple(typeAndroid).then(response => {
        if (
            response.photo !== 'authorized' ||
            response.microphone !== 'authorized'
        ) {
          this.requestPermission();
        }
        if (response.photo === 'authorized') {
          this.setState({ permissionPhoto: true });
          this.initAudio();
        }
        if (response.microphone === 'authorized') {
          this.setState({ permissionRecord: true });
          this.initAudio();
        }
      });
    } else {
      Permissions.checkMultiple(typeIos).then(response => {
        if (
            response.photo !== 'authorized' ||
            response.microphone !== 'authorized' ||
            response.notification !== 'authorized'
        ) {
          this.requestPermission();
        }
        if (response.photo === 'authorized') {
          this.setState({ permissionPhoto: true });
          this.initAudio();
        }
        if (response.microphone === 'authorized') {
          this.setState({ permissionRecord: true });
          this.initAudio();
        }
      });
    }
  };

  registerPushToken = async() => {
    // get firebase cloud messaging token
    const fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
        this.pushToken = fcmToken;
        this.setState({tokenLoaded: true}, () => {
          if (this.state.webviewLoaded) {
            this.webviewPostToken();
          }
        });
    }
  };
  webviewPostToken() {
    this.webview.postMessage(JSON.stringify({
      action: 'registerPushToken',
      token: this.pushToken,
      type: Platform.OS === 'ios'
        ? 0 : Platform.OS === 'android'
        ? 1 : null,
      os_version: Platform.Version,
      app_version: DeviceInfo.getReadableVersion()
    }));
  }

  onImagePress = uri => {
    this.file = {
      uri,
      name: new Date().getTime(),
      type: `image/jpeg`,
    };
    this.setState({ visible: false, imagePicked: uri });
    ImageResizer.createResizedImage(uri, 200, 200, 'JPEG', 100)
      .then(({ uri }) => {
        NativeModules.ImageStoreManager.getBase64ForTag(
          uri,
          base64 => {
            this.webview.postMessage(base64);
          },
          err => {},
        );
      })
      .catch(err => {
        return Alert.alert(
          'Unable to resize the photo',
          'Check the console for full the error message',
        );
      });
  };
  handleFirstConnectivityChange = info => {
    if (info.type === 'none') {
      this.setState({ open: true });
    } else {
      this.setState({ open: false });
    }
  };
  handleOpenURL = event => {
    const route = event.url.replace(/.*?:\/\//g, '');
  };
  initAudio = () => {
    this.prepareRecordingPath(this.state.audioPath);
    AudioRecorder.onProgress = data => {
      this.setState({ currentTime: Math.floor(data.currentTime) });
    };
    if (Platform.OS === 'ios') {      
      AudioRecorder.onFinished = data => {
        this.finishRecording(
          data.status === 'OK',
          data.audioFileURL,
          data.audioFileSize,
        );
      };

      /*
      AudioRecorder.onFinished = function(data) {
        this.finishRecording(
          data.status === 'OK',
          data.audioFileURL,
          data.audioFileSize,
        );
        this.filePath = data.audioFileURL;
      };
      */
    }
  };
  prepareRecordingPath(audioPath) {
    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: 'Low',
      AudioEncoding: 'aac',
      AudioEncodingBitRate: 32000,
    });
  }

  requestPermission = () => {
    Permissions.request('photo')
      .then(response => {
        this.setState({ permissionPhoto: response === 'authorized' });
        return Permissions.request('microphone');
      })
      .then(response => {
        this.setState({ permissionRecord: response === 'authorized' });
        this.initAudio();
        return Permissions.request('notification', { type: ['alert', 'badge'] });
      })
      .then(response => {
        this.setState({ permissionPush: response === 'authorized' });
      });
  };

  onClose = () => this.setState({ visible: false, testmodal: false });

  pause = async () => {
    if (!this.state.recording) {
      return;
    }
    try {
      const filePath = await AudioRecorder.pauseRecording();
      this.setState({ paused: true });
    } catch (error) {
      console.error(error);
    }
  };

  resume = async () => {
    if (!this.state.paused) {
      console.warn("Can't resume, not paused!");
      return;
    }

    try {
      await AudioRecorder.resumeRecording();
      this.setState({ paused: false });
    } catch (error) {
      console.error('resume', error);
    }
  };

  stop = async url => {
    if (!this.state.recording && !this.filePath) {
      return;
    }

    this.setState({ stoppedRecording: true, recording: false, paused: false });
    if (this.state.finished && this.filePath) {
      return this.filePath;
    }
    try {
      const filePath = await AudioRecorder.stopRecording();
      // window.postMessage()

      this.setState({ finished: true });
      if (Platform.OS === 'android') {
        this.finishRecording(true, filePath);
        this.filePath = `file://${filePath}`;
        return `file://${filePath}`;
      } else {
        
        
        return (AudioRecorder.onFinished = data => {
          console.log('this.filePath', this.filePath);
          console.log('audioFileURL', data.audioFileURL);

          this.filePath = data.audioFileURL;
          return data.audioFileURL;
        });
        
        /*
        return (AudioRecorder.onFinished = function(data) {
          console.log('filePath', this.filePath)
          console.log('data.audioFileURL', data.audioFileURL)

          this.filePath = data.audioFileURL;
          return data.audioFileURL;
        });
        */

        /*
        return (AudioRecorder.onFinished = function(data) {
          this.finishRecording(
            data.status === 'OK',
            data.audioFileURL,
            data.audioFileSize,
          );
          this.filePath = data.audioFileURL;
          return data.audioFileURL;
        });
        */
      }
    } catch (error) {
      console.error(error);
      throw new Error('error');
    }
  };
  upload = filePath => {
    if (filePath)
      this.uploadToS3({
        uri: filePath,
        name: `audio/${new Date()
          .toISOString()
          .slice(0, 19)
          .replace(/:/g, '')
          .replace(/-/g, '')}`,
        type: 'audio/mp3',
      });
  };

  play = async () => {
    if (this.state.recording) {
      await this.stop();
    }
    this.webview.postMessage(WebViewTypes.PLAY);
    this.sound.play(success => {
      if(success) {
        this.setState({ playing: false });
        this.webview.postMessage(WebViewTypes.STOP_SOUND);
      }
    })
  };

  record = async () => {
    this.initAudio();
    if (this.state.recording) {
      console.warn('Already recording!');
      return;
    }

    if (!this.state.permissionRecord) {
      console.warn("Can't record, no permission granted!");
      return;
    }

    if (this.state.stoppedRecording) {
      this.prepareRecordingPath(this.state.audioPath);
    }

    this.setState({ recording: true, paused: false });

    try {
      const filePath = await AudioRecorder.startRecording();
    } catch (error) {}
  };
  finishRecording = function(didSucceed, filePath, fileSize){
    this.setState({ finished: didSucceed });
  };
  destroy = () => {
    this.setState({
      visible: false,
      paused: false,
      audioPath: audioPath,
      testmodal: false,
      currentTime: 0.0,
      recording: false,
      stoppedRecording: false,
      finished: false,
      open: false,
      playing: false,
    });
    if (this.sound) {
      this.sound.pause();
      this.sound.release()
    }
    this.file = undefined;
    this.filePath = undefined;
  };

  uploadToS3 = file => {
    const { imagePicked, API } = this.state;
    let params = new FormData();
    params.append('file', file);
    fetch(API + "pushfile", {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${this.token}`,
        'Content-Type': 'multipart/form-data',
      },
      body: params
    }).then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        this.setState({ finished: false });
        this.filePath = undefined;
        if(imagePicked) {
          this.setState({ imagePicked: undefined });
        }
        else {
          this.sound = new Sound(responseJson.signed_url, '', error => {
            if (error) {
              //alert("Fail to open audio file")
            }
          });
        }
        this.webview.postMessage(responseJson.url);
      })
      .catch((error) => {
        console.log(error)
      });
  };

  onMessage = async event => {
    const {permissionPhoto, permissionRecord} = this.state;
    switch (event.nativeEvent.data) {
      case WebViewTypes.CAMERA: {
        if (permissionPhoto)
          this.setState({
            visible: true,
          });
        else {
          Permissions.request('photo').then(response => {
            if (response === 'authorized') {
              this.setState({
                permissionPhoto: true,
                visible: true,
              });
            } else {
              Alert.alert(
                  'Warning',
                  'Please accept photo permission to use this feature!',
              );
            }
          });
        }
        break;
      }
      case WebViewTypes.RECORD: {
        if (permissionRecord) {
          await this.record();
        } else {
          Permissions.request('microphone').then(response => {
            if (response === 'authorized') {
              this.setState({
                permissionRecord: true,
              });
            } else {
              Alert.alert(
                  'Warning',
                  'Please accept microphone permission to use this feature!',
              );
            }
          });
        }
        break;
      }
      case WebViewTypes.PAUSE_SOUND: {
        if (this.sound) {
          this.sound.pause();
        }
        break;
      }
      case WebViewTypes.PAUSE: {
        if (permissionRecord) {
          await this.pause();
        } else {
          Permissions.request('microphone').then(response => {
            if (response === 'authorized') {
              this.setState({
                permissionRecord: true,
              });
            } else {
              Alert.alert(
                  'Warning',
                  'Please accept microphone permission to use this feature!',
              );
            }
          });
        }
        break;
      }
      case WebViewTypes.DESTROY: {
        this.destroy();
        break;
      }
      case WebViewTypes.STOP_SOUND: {
        if (this.sound) {
          this.sound.stop();
        }
        break;
      }
      case WebViewTypes.STOP: {
        if (permissionRecord) {
          await this.stop();
        } else {
          Permissions.request('microphone').then(response => {
            if (response === 'authorized') {
              this.setState({
                permissionRecord: true,
              });
            } else {
              Alert.alert(
                  'Warning',
                  'Please accept microphone permission to use this feature!',
              );
            }
          });
        }
        break;
      }
      case WebViewTypes.POST: {
        this.stop().then(res => {
          this.upload(res);
        });
        break;
      }
      case WebViewTypes.RESUME: {
        this.resume();
        break;
      }
      case WebViewTypes.PUT_IMAGE: {
        this.uploadToS3(this.file);
        break;
      }
      case WebViewTypes.PLAY: {
        await this.play();
        break;
      }
      default: {
        const data_raw = event.nativeEvent.data;
        if (data_raw.search(WebViewTypes.FILE_URL) !== -1) {
          let regex = new RegExp(WebViewTypes.FILE_URL, 'g');
          let file_url = data_raw.replace(regex, '');
          if (file_url != 'undefined') {
            this.sound = new Sound(file_url, '', error => {
              if (error) {
                //alert("Fail to open audio file")
              }
            });
          }
        } else if (data_raw.search(WebViewTypes.OPEN_URL) !== -1) {
          let regex = new RegExp(WebViewTypes.OPEN_URL, 'g');
          let url = data_raw.replace(regex, '');
          Linking.openURL(url).catch(err =>
              console.error('An error occurred', err),
          );
        } else if (data_raw.search(WebViewTypes.USER) !== -1) {
          if (!this.token) {
            this.token = data_raw.replace(WebViewTypes.USER, '')
          }
        } else if (data_raw.search(WebViewTypes.TAB) !== -1) {
          let tab = data_raw.replace(WebViewTypes.TAB, '');
          this.setState({
            currentTab: tab
          })
        }
        break;
      }
    }
  };

  render() {
    const { visible, open } = this.state;
    return (
      <View
        style={{
          flex: 1,
          paddingTop: Platform.OS === 'ios' ? statusBarHeight : 0,
        }}
      >
        <WebView
          style={{ flex: 1 }}
          ref={com => {
            this.webview = com;
          }}
          javaScriptEnabled
          source={{ uri: this.state.webviewURI }}
          onMessage={this.onMessage}
          //injectedJavaScript={`(${String(this.patchPostMessageFunction)})();`}
          allowUniversalAccessFromFileURLs
          allowsInlineMediaPlayback
          //scalesPageToFit
          bounces={false}
          onLoadEnd={() => {
            this.setState({ webviewLoaded: true }, () => {
              if (this.state.tokenLoaded && this.pushToken) {
                this.webviewPostToken();
              }
            });
          }}
        />

        <Modal
          animationType="slide"
          visible={open}
          transparent
          onRequestClose={() => {}}
        >
          <ModalNetwork visible={open} />
        </Modal>
        <CameraModal
          onRequestClose={this.onClose}
          onClose={this.onClose}
          visible={visible}
          imagePress={this.onImagePress}
        />
      </View>
    );
  }
}

export default App;
