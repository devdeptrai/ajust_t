import React, { PureComponent } from 'react';
import {
  Modal,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Platform,
  CameraRoll,
  Image,
  Dimensions,
} from 'react-native';

const { width } = Dimensions.get('window');

export default class CameraModal extends PureComponent {
  state = {
    photos: [],
    page: 1,
    refreshing: false,
  };
  componentDidMount() {}
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.visible &&
      this.state.photos.length === 0 &&
      !this.state.refreshing
    ) {
      this.loadImage();
    }
  }
  loadImage = () => {
    const { photos } = this.state;

    if (CameraRoll && photos.length === 0) {
      CameraRoll.getPhotos({
        first: 20,
        assetType: 'All',
      }).then(r => {
        this.setState({
          photos: r.edges,
        });
      });
    }
  };

  onEndReached = () => {
    const { page } = this.state;
    CameraRoll.getPhotos({
      first: 20 * page,
      assetType: 'All',
    }).then(r => {
      if (r.page_info.has_next_page) {
        this.setState({
          photos: r.edges,
          visible: true,
          page: page + 1,
          refreshing: true,
        });
      }
    });
  };

  renderItem = ({ item }) => (
    <TouchableOpacity
      onPress={() => {
        this.props.imagePress(item.node.image.uri);
      }}
    >
      <Image
        source={{ uri: item.node.image.uri }}
        style={{
          width: width * 0.3,
          height: width * 0.3,
          marginLeft: width * 0.025,
          marginTop: width * 0.025,
        }}
      />
    </TouchableOpacity>
  );

  render() {
    const { visible } = this.props;
    const { refreshing, photos, page } = this.state;

    return (
      <Modal visible={visible} style={{ flex: 1 }} onRequestClose={() => {}}>
        <View
          style={{
            width: '100%',
            height: 50,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: Platform.OS === 'ios' ? 22 : null,
          }}
        >
          <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Camera</Text>
          <TouchableOpacity
            style={{
              position: 'absolute',
              right: 20,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => this.props.onClose()}
          >
            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>X</Text>
          </TouchableOpacity>
        </View>
        <FlatList
          data={photos}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
          numColumns={3}
          refreshing={refreshing}
          onEndReached={this.onEndReached}
          onEndReachedThreshold={photos.length > 20 ? 1 : 0.2}
        />
      </Modal>
    );
  }
}
