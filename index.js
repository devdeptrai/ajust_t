/** @format */

import { AppRegistry } from 'react-native';
import App from './src';
import { name as appName } from './app.json';
// import AudioExample from './src/audio'
AppRegistry.registerComponent(appName, () => App);
